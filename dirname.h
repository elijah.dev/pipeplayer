#ifndef __DIRNAME_H__
#define __DIRNAME_H__

/* Get directory from path
 * Return length, store in buffer */
size_t dirname(const char *path, char **buffer);

#endif /* __DIRNAME_H__ */
