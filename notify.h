#ifndef __NOTIFY_H__
#define __NOTIFY_H__

#include <libnotify/notify.h>

static NotifyNotification *notification;
static GError *notification_error = NULL;

void init_notification();
void notify(const char *string);

#endif /* __NOTIFY_H__ */
