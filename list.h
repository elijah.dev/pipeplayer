#ifndef __LIST_H__
#define __LIST_H__

// Linked list implementation

typedef struct node {
  char *str;
  struct node *next;
} node_t;

node_t *new_node(node_t *previous);
void free_list(node_t *root);
void node_pop_first(node_t **root);

#endif /* __LIST_H__ */
