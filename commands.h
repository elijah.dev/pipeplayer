#ifndef __COMMANDS_H__
#define __COMMANDS_H__

#define EXIT     'e'
#define LOAD     'l'
#define FORWARD  'f'
#define BACKWARD 'b'
#define PAUSE    'p'
#define CLEAR    'c'
#define NEWLINE  '\n'

#endif /* __COMMANDS_H__ */
