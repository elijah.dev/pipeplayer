#ifndef __PIPE_H__
#define __PIPE_H__

#include <stddef.h>

size_t pipe_readline(char **buffer_ptr, char *pipe_path);

#endif /* __PIPE_H__ */
