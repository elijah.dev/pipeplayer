#ifndef __PLAYLIST_H__
#define __PLAYLIST_H__

#include <stdbool.h>
#include <stddef.h>
#include "config.h"

struct playlist {
  char *path, *dir, **array;
  size_t index, length;
  bool need_free, need_cancel;
#if NOTIFICATION == 1
  char **names_array;
#endif
};

void free_playlist(struct playlist *);
void load_playlist(struct playlist *);

#endif /* __PLAYLIST_H__ */
