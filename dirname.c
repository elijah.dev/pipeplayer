/* Put dirname with slash in buffer
 * Return length of buffer */

#include <stddef.h>
#include <string.h>
#include <stdlib.h>

size_t
dirname(const char *path, char **buffer_ptr)
{
  size_t index = 0, slash_index = 0;

  // Find slash
  for (; path[index] != '\0'; index++)
    if (path[index] == '/')
      slash_index = index;

  // If not slashes or one at the beginning, then / is dirname
  if (slash_index == 0) {
    *buffer_ptr = (char *) malloc(2);
    strcpy(*buffer_ptr, "/");
    return (size_t)1;
  }
  
  // Copy string
  slash_index++;
  *buffer_ptr = (char *) malloc(slash_index + 1);
  strncpy(*buffer_ptr, path, slash_index);
  (*buffer_ptr)[slash_index] = '\0';
  return slash_index;
}
