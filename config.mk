# Customize below to fit your system
PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man

# Name of program
VERSION = 1.4.1
NAME = pipeplayer

# Compiler
CC = tcc

PKG_CONFIG = pkg-config

LIBS = -lpthread `$(PKG_CONFIG) -libs libmpg123 ao`

# Options
NOTIFICATION = 1

# Notification support
ifeq ($(NOTIFICATION),1)
	NOTIFY_LIBS = `$(PKG_CONFIG) -libs libnotify`
	NOTIFY_INCS = `$(PKG_CONFIG) -cflags libnotify`
	NOTIFY_SRC  = notify.c
endif

# Flags
CFLAGS    = -O2 -Wall $(NOTIFY_INCS)
LDFLAGS   = -O2 -s $(LIBS) $(NOTIFY_LIBS)
