#include <stdlib.h>
#include "list.h"

node_t *
new_node(node_t *previous)
{
  node_t *node = (node_t *) malloc(sizeof(node_t));
  node->next = NULL;
  if (previous != NULL)
    previous->next = node;
  return node;
}

void
free_list(node_t *root)
{
  while (root != NULL) {
    node_t *previous = root;
    root = root->next;
    free(previous);
  }
}

void
node_pop_first(node_t **root)
{
  node_t *current = *root;
  *root = current->next;
  free(current);
}
