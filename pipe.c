#include <stdio.h>
#include <stdlib.h>

#include "pipe.h"

size_t
pipe_readline(char **buffer_ptr, char *pipe_path)
{
  size_t len;
  char *line = NULL;
  FILE *fd = fopen(pipe_path, "r");
  size_t read = getline(&line, &len, fd);
  line[--read] = '\0';
  fclose(fd);
  *buffer_ptr = line;
  return read;
}
