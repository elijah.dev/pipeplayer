#ifndef __LOG_H__
#define __LOG_H__

#include "loglevels.h"
#include "config.h"

// Default logging level is error
#ifndef LOGGING_LEVEL
#define LOGGING_LEVEL ERROR
#endif /* Logging level */

// Colors
#if LOGGING_COLORS == 1
#define __LOGGING_ERROR_MESSAGE "[91m[ERROR](B[m "
#define __LOGGING_INFO_MESSAGE  "[92m[INFO](B[m  "
#define __LOGGING_DEBUG_MESSAGE "[94m[DEBUG](B[m "
#else
#define __LOGGING_ERROR_MESSAGE "[ERROR] "
#define __LOGGING_INFO_MESSAGE  "[INFO]  "
#define __LOGGING_DEBUG_MESSAGE "[DEBUG] "
#endif /* Colors */

// Logging macro and include of stdio
#if LOGGING_LEVEL >= ERROR
#include <stdio.h>
#define LOG_ERROR(...) printf(__LOGGING_ERROR_MESSAGE); printf(__VA_ARGS__); puts("");
#else
#define LOG_ERROR(...)
#endif

#if LOGGING_LEVEL >= INFO 
#define LOG_INFO(...)  printf(__LOGGING_INFO_MESSAGE);  printf(__VA_ARGS__); puts("");
#else
#define LOG_INFO(...)
#endif

#if LOGGING_LEVEL >= DEBUG 
#define LOG_DEBUG(...) printf(__LOGGING_DEBUG_MESSAGE); printf(__VA_ARGS__); puts("");
#else
#define LOG_DEBUG(...)
#endif

#endif /* __LOG_H__ */
