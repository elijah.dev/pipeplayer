# pipeplayer - simple music player

.POSIX:

include config.mk

SRC = pipeplayer.c dirname.c player.c playlist.c list.c threads.c pipe.c $(NOTIFY_SRC)
OBJ = $(SRC:.c=.o)

all: options $(NAME)

options:
	@echo $(NAME) build options:
	@echo "CFLAGS  = $(CFLAGS)"
	@echo "LDFLAGS = $(LDFLAGS)"
	@echo "CC      = $(CC)"

.c.o:
	$(CC) $(CFLAGS) -c $<

log.h: config.h loglevels.h
threads.o: commands.h playlist.h player.h log.h threads.h pipe.h config.h
playlist.o: playlist.h dirname.h list.h log.h config.h
player.o: player.h config.h log.h
pipeplayer.o: player.h playlist.h threads.h pipe.h config.h
pipe.o: pipe.h
list.o: list.h

$(OBJ): config.mk

$(NAME): $(OBJ)
	$(CC) -o $@ $(LDFLAGS) $(OBJ)

clean:
	rm -f $(NAME) $(OBJ)

install: $(NAME)
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f $(NAME) $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/$(NAME)
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	sed "s/VERSION/$(VERSION)/g" < $(NAME).1 > $(DESTDIR)$(MANPREFIX)/man1/$(NAME).1
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/$(NAME).1

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/$(NAME)
	rm -f $(DESTDIR)$(MANPREFIX)/man1/$(NAME).1
