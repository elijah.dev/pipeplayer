#ifndef __THREADS_H__
#define __THREADS_H__

#include <pthread.h>
#include "player.h"
#include "playlist.h"

struct thread_arg {
  struct mpg_player *player;
  struct playlist *playlist;
  pthread_t *thread_player;
  char *pipe_path;
};

// Thread function
void run_threads(struct mpg_player *player, struct playlist *playlist, char *pipe_path);
void *play_thread(void *vargp);
void *input_thread(void *vargp);

#endif /* __THREADS_H__ */
