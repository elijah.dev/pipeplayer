#ifndef __CONFIG_H__
#define __CONFIG_H__

#define BITS 8

// Logging options
#include "loglevels.h"

/* 0 - off colors
 * 1 - on colors */
#define LOGGING_COLORS 1

// Levels (see log.h)
#define LOGGING_LEVEL INFO

/* 0 - don't show notifications
 * 1 - show notification 
 * You must compile with NOTIFICATION=1 to enable this */
#define NOTIFICATION 1
/* 0 - don't show song number in notifications
 * 1 - show song number in notifications */
#define SHOW_NUMBER 1

// Notification information
#define APP_NAME "Pipeplayer"
#define SUMMARY  "Pipeplayer"

#endif /* __CONFIG_H__ */
