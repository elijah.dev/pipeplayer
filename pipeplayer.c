#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "player.h"
#include "playlist.h"
#include "threads.h"
#include "pipe.h"
#include "config.h"

#if NOTIFICATION == 1
#include "notify.h"
#endif

// Function on Ctrl-C
void
sigint_handler(int _)
{
  puts("Use 'e' command to exit");
}

// Entry point
int
main(int argc, char *argv[])
{
  // Usage information
  char *pipe, *xdg_runtime_dir;

  switch (argc) {
    // Path is provided
    case 2:
      pipe = strdup(argv[1]);
      break;

    // If no arguments provided, pipe = $XDG_RUNTIME_DIR/pipeplayer
    case 1:
      if ((xdg_runtime_dir = getenv("XDG_RUNTIME_DIR")) != NULL) {
        pipe = malloc(strlen(xdg_runtime_dir) + 12);  // Plus length of "/pipeplayer" plus "\0"
        strcpy(pipe, xdg_runtime_dir);
        strcat(pipe, "/pipeplayer");
        break;
      }
      puts("XDG_RUNTIME_DIR variable must be set or path to fifo must be specified");
      // The go to default case

    default:
      printf("Usage: %s        - creates FIFO on $XDG_RUNTIME_DIR/pipeplayer", argv[0]);
      printf("       %s [path] - creates FIFO on given path\n", argv[0]);
      return 1;
  }

  if (access(pipe, F_OK) != 0 && mkfifo(pipe, 0644) != 0) {
    printf("Cannot create fifo at %s\n", pipe);
    return 1;
  }

  // mpg player
  struct mpg_player player, *player_ptr = &player;
  player.ao.need_close_device = false;
  player.playing = false;

  // playlist
  struct playlist playlist = {
    .need_free   = false,
    .need_cancel = false,
    .index       = 0,
    .length      = 0,
    .path        = NULL,
  }, *playlist_ptr = &playlist;

  // Set up Ctrl-C handler
  signal(SIGINT, sigint_handler);

  // Initialize player
  init_player(player_ptr);

#if NOTIFICATION == 1
  // Initialize notification  
  init_notification();
#endif

  // Create and join input thread
  run_threads(player_ptr, playlist_ptr, pipe);

  // Clear player and playlist
  free_player(player_ptr);
  free_playlist(playlist_ptr);
  free(pipe);

  return 0;
}
