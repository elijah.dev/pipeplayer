#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "playlist.h"
#include "dirname.h"
#include "list.h"
#include "log.h"
#include "config.h"

#if NOTIFICATION == 1
#include "notify.h"
#endif

void
free_playlist(struct playlist *playlist)
{
  if (!playlist->need_free)
    return;

  LOG_DEBUG("Clearing playlist");

  // Clear array items
  for (playlist->index = 0; playlist->index < playlist->length; playlist->index++)
    free(playlist->array[playlist->index]);

  // Clear array(s)
  free(playlist->array);
#if NOTIFICATION == 1
  free(playlist->names_array);
#endif

  // Clear playlist dir
  free(playlist->dir);

  playlist->need_free = false;
}

void
_copy_list_to_array(node_t **current, char **array, size_t count)
{
  node_t *list = *current;
  
  for (size_t index = 0; index < count; index++) {
    *current = (*current)->next;
    array[index] = (*current)->str;
  }

  free(list);
}

void
load_playlist(struct playlist *playlist)
{
  FILE *fp;
  ssize_t read;
#if NOTIFICATION == 1 && SHOW_NUMBER == 1
  size_t song_name_length;
#endif
  size_t length, len, count = 0;
  char *line = NULL;

  // Clear previous playlist
  free_playlist(playlist);

  LOG_INFO("Loading playlist from %s", playlist->path);

  // Read file setup
  fp = fopen(playlist->path, "r");
  playlist->index = 0;

  // Return if cannot open file or first line is empty
  if (fp == NULL) {
    LOG_ERROR("Cannot load playlist from %s", playlist->path);
    playlist->need_free = false;
    playlist->length = 0;
    return;
  }

  // Get dirname and length of it
  length = dirname(playlist->path, &playlist->dir);

  node_t *list = new_node(NULL),
         *current = list;

#if NOTIFICATION == 1
  node_t *names_list = new_node(NULL),
         *current_name = names_list;
#endif

  // Read lines
  while ((read = getline(&line, &len, fp)) != EOF) {
    // "read" is length of string
    current = new_node(current);
    line[read - 1] = '\0';

    current->str = (char *) malloc(read + length);
    strncpy(current->str, playlist->dir, length);
    strncpy(current->str + length, line, read);

    count++;
    
#if NOTIFICATION == 1
#if SHOW_NUMBER == 1
    song_name_length = count / 10 + read;
    current_name = new_node(current_name);
    current_name->str = (char *) malloc(song_name_length);
    snprintf(current_name->str, song_name_length - 1, "%lu. %s", count, line);
#else
    current_name = new_node(current_name);
    current_name->str = (char *) malloc(read - 4);
    strncpy(current_name->str, line, read - 5);
#endif /* SHOW_NUMBER == 1 */
#endif /* NOTIFICATION == 1*/
  }

  // Set playlist values
  playlist->need_free = true;
  playlist->length = count;

  // Allocate array(s) and copy strings
  playlist->array = (char **) malloc(count * sizeof(char *));
  _copy_list_to_array(&list, playlist->array, count);

#if NOTIFICATION == 1
  playlist->names_array = (char **) malloc(count * sizeof(char *));
  _copy_list_to_array(&names_list, playlist->names_array, count);
#endif

  // Close file and free line
  fclose(fp);
  if (line)
    free(line);
}
