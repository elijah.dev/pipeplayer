#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <stdbool.h>
#include <stddef.h>
#include <mpg123.h>
#include <ao/ao.h>

struct audio_output {
  ao_device *device;
  bool need_close_device;
  ao_sample_format format;
  int channels, encoding;
  int driver;
  long rate;
};

struct mpg_player {
  mpg123_handle *handler;
  char *buffer;
  size_t buffer_size, done;
  int error;
  bool playing;
  struct audio_output ao;
};

void init_player(struct mpg_player *);
void free_player(struct mpg_player *);
void prepare_to_play(struct mpg_player *, const char *path);
void play(struct mpg_player *);

#endif /* __PLAYER_H__ */
