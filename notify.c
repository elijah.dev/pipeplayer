#include "notify.h"
#include "config.h"

void
init_notification()
{
  notify_init(APP_NAME);
  notification = notify_notification_new(SUMMARY, "", NULL);
}

void
notify(const char *string)
{
  notify_notification_update(notification, SUMMARY, string, NULL);
  notify_notification_show(notification, &notification_error);
}
