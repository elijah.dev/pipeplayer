#include <mpg123.h>
#include <ao/ao.h>
#include <stdbool.h>
#include <stdlib.h>

#include "player.h"
#include "config.h"
#include "log.h"

void
init_player(struct mpg_player *player)
{
  LOG_DEBUG("Initializing player");

  // Audio output
  ao_initialize();
  player->ao.driver = ao_default_driver_id();
  // Mpg123
  mpg123_init();
  player->handler = mpg123_new(NULL, &player->error);
  player->buffer_size = mpg123_outblock(player->handler);
  player->buffer = (char*) malloc(player->buffer_size * sizeof(unsigned char));
}

void
free_player(struct mpg_player *player)
{
  LOG_DEBUG("Clearing memory of player");

  free(player->buffer);

  if (player->ao.need_close_device)
    ao_close(player->ao.device);

  mpg123_close(player->handler);
  mpg123_delete(player->handler);
  mpg123_exit();

  ao_shutdown();
}

void
prepare_to_play(struct mpg_player *player, const char *path)
{
  LOG_DEBUG("Preparing to play %s", path);

  struct audio_output *ao = &player->ao;
  ao_sample_format *format = &ao->format;

  mpg123_open(player->handler, path);
  mpg123_getformat(
    player->handler, 
    &ao->rate, 
    &ao->channels, 
    &ao->encoding
  );

  /* Set the output format and open the output device */
  format->bits = mpg123_encsize(ao->encoding) * BITS;
  format->rate = ao->rate;
  format->channels = ao->channels;
  format->byte_format = AO_FMT_NATIVE;
  format->matrix = 0;
  ao->device = ao_open_live(ao->driver, format, NULL);
  ao->need_close_device = true;
}

void
play(struct mpg_player *player)
{
  /* Open the file and get the decoding format */
  struct audio_output *ao = &player->ao;

  player->playing = true;

  /* Decode and play */
  while (mpg123_read(
    player->handler, 
    player->buffer, 
    player->buffer_size, 
    &player->done
  ) == MPG123_OK && player->playing)
    ao_play(ao->device, player->buffer, player->done);
}
