#ifndef __LOG_LEVELS_H__
#define __LOG_LEVELS_H__

/* Logging levels 
 * NOLOG means no log messages at all
 * ERROR is error messages
 * INFO provides error and info messages
 * DEBUG provides all log messages */
#define NOLOG 0
#define ERROR 1
#define INFO  2
#define DEBUG 3

#endif /* __LOG_LEVELS_H__ */
