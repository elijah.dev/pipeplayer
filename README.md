# Pipeplayer

Pipeplayer is a simple music daemon. It uses libmpg123 to decode files and libao to play sound. You can control it with named pipe (FIFO).

## Before using this

This program requires *playlist* file which is plain text of songs in the same directory.
For example, you have folder with this files: `song1.mp3`, `song_2.mp3`, `4.mp3` and `56.mp3`.
Your playlist file shoul'd look like:
```
song1.mp3
song_2.mp3
4.mp3
56.mp3
```
**Tip**: cd into folder with music and run command `ls -1N | tee playlist` to automatically generate playlist file. Note that order may differ.

## Usage

First, you need to check your XDG_RUNTIME_DIR variable
```
echo $XDG_RUNTIME_DIR
```

If you see no output, then you need to export variable:

On bash, zsh etc.:
```
export XDG_RUNTIME_DIR="$HOME/.local/runtime"
```
On fish:
```
set -x XDG_RUNTIME_DIR "$HOME/.local/runtime"
```

Then make sure directory $XDG_RUNTIME_DIR exists:
```
mkdir -p "$XDG_RUNTIME_DIR"
```

Then, you can start daemon either during start of your desktop environment or window manager or even in tty with this command:
```
pipeplayer
```
Note: add `&` at the end of command to run process in background.

Or if you prefer old way:
```
pipeplayer /path/to/named/pipe
```

Now you can send commands to created named pipe like this:
```
echo l$HOME/Music/playlist > "$XDG_RUNTIME_DIR/pipeplayer"
```
Note: change "$XDG_RUNTIME_DIR/pipeplayer" to path to named pipe if you don't use XDG_RUNTIME_DIR variable.

### Available commands

* `l/path/to/playlist` — load playlist from /path/to/playlist
* `b` — play previous song
* `f` — play next song
* `e` — exit pipeplayer
* `p` — pause
* `c` — clear playlist (don't exit)

Also, you can find all commands in man page. Run `man pipeplayer` after installation.

# Installation

You need a few things:
* git
* C compiler, such as **gcc**, **clang** or **tcc**
* make
* libmpg123
* libao

Install that stuff, clone this repository, cd into it and simply run make and sudo (or doas) make install:

```
git clone https://git.disroot.org/elijah.dev/pipeplayer.git
cd pipeplayer
make
sudo make install
```
If you prefer gcc, then run `make CC=gcc` instead of `make`.
Also, if you prefer clang, run `make CC=clang.`

Replace `make` with `make NOTIFICATION=0` if you want to disable notification.
Make sure you change NOTIFICATION variable in `config.h`.

Here some tips for installing dependencies on some distros:

### Archlinux and arch-based distros
```
pacman -Syu --needed tcc mpg123 libao
```
* Install `libnotify` package for notification support

Guides for other distibutions will appear in future. If you know one, feel free to add.

## TODO

- [x] Add pause command
- [x] Add clear playlist command
- [ ] Add navigation inside song
- [x] Add notification support

## Bugs

If you have found bugs, you can open issue or contact me via email elijah.dev@disroot.org
